package work.huangxin.share.util;

import io.jsonwebtoken.lang.Assert;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ClassUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.BeansException;
import org.springframework.beans.FatalBeanException;

import javax.servlet.ServletResponse;
import java.beans.PropertyDescriptor;
import java.io.IOException;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author yu.li
 * @Description DingDing客户端接口
 * @Date 2020/5/714:15
 */
@Slf4j
public class ParamsUtil extends BeanUtils {


    public static boolean collectIsEmpty(Collection<?> collection) {
        return collection == null || collection.isEmpty();
    }

    public static boolean containsKey(Map map, Object key) {
        if (map == null || key == null) {
            return false;
        }
        if (!map.containsKey(key)) {
            return false;
        }
        return true;
    }

    public static <T> T getMapValByKey(Map<String, T> map, String key) {
        if (!containsKey(map, key)) {
            return null;
        }
        return map.get(key);
    }

    public static <T> T getMapValByKey(Map<Object, T> map, Object key) {
        if (!containsKey(map, key)) {
            return null;
        }
        return map.get(key);
    }

    public static String getMapValByKey(Map<String, String> templateMap, Map<String, String> key) {
        if (!containsKey(templateMap, key)) {
            return null;
        }
        return templateMap.get(key);
    }

    public static <T> List<T> getMapListByKey(Map<String, List<T>> map, Object key) {
        if (!containsKey(map, key)) {
            return null;
        }
        return map.get(key);
    }

    public static <T> List<T> getMapListByKey(Map<Integer, List<T>> map, Integer key) {
        if (!containsKey(map, key)) {
            return null;
        }
        return map.get(key);
    }

    public static boolean isBlockVal(Integer val) {
        return val == null || val.intValue() < 1;
    }

    public static boolean isBlockVal(Long val) {
        return val == null || val.intValue() < 1;
    }

    public static String getVal(String val) {
        return isBlank(val) ? null : val;
    }

    public static int getVal(Integer val) {
        return val == null ? 0 : val.intValue();
    }

    public static int length(final CharSequence cs) {
        return cs == null ? 0 : cs.length();
    }

    public static boolean isBlank(final CharSequence cs) {
        final int strLen = length(cs);
        if (strLen == 0) {
            return true;
        }
        for (int i = 0; i < strLen; i++) {
            if (!Character.isWhitespace(cs.charAt(i))) {
                return false;
            }
        }
        return true;
    }

    public static String Int2Score(Integer val) {
        if (val == null || val < 1) {
            return "0";
        }
        BigDecimal amtDecimal = BigDecimal.valueOf(val).divide(new BigDecimal(100));
        DecimalFormat decimalFormat = new DecimalFormat("0.#");
        return decimalFormat.format(amtDecimal);
    }

    public static String Long2Score(Long val) {
        if (val == null) {
            return "0";
        }
        BigDecimal amtDecimal = BigDecimal.valueOf(val).divide(new BigDecimal(100));
        DecimalFormat decimalFormat = new DecimalFormat("#0.00");
        return decimalFormat.format(amtDecimal);
    }

    public static String Int2ScoreThousand(Integer val) {
        if (val == null) {
            return "0";
        }
        BigDecimal amtDecimal = BigDecimal.valueOf(val).divide(new BigDecimal(100));
        DecimalFormat decimalFormat = new DecimalFormat("#0.00");
        return decimalFormat.format(amtDecimal);
    }

    public static Integer score2Int(String amt) {
        BigDecimal result = new BigDecimal(amt).multiply(new BigDecimal(100));
        return result.intValue();
    }

    public static long score2Long(String amt) {
        BigDecimal result = new BigDecimal(amt).multiply(new BigDecimal(100));
        return result.longValue();
    }

    public static String stripHTML(String strHtml) {
        String txtcontent = strHtml.replaceAll("</?[^>]+>", ""); //剔出<html>的标签
        txtcontent = txtcontent.replaceAll(" ", "");//去除空格
        txtcontent = txtcontent.replaceAll("&nbsp;", "");//去除空格
        txtcontent = txtcontent.replaceAll("<script>\\s*|\t|\r|\n</script>", "");//去除空格
        txtcontent = txtcontent.replaceAll("<a>\\s*|\t|\r|\n</a>", "");//去除字符串中的空格,回车,换行符,制表符
        return txtcontent;
    }

    public static String getSubTextStr(String source, int len) {
        if (StringUtils.isEmpty(source)) {
            return "";
        }
        source = stripHTML(source);
        if (source.length() < len) {
            return source;
        }
        return source.substring(0, len);
    }

    public static String getSubStr(String source, int len) {
        if (StringUtils.isEmpty(source)) {
            return "";
        }
        if (source.length() < len) {
            return source;
        }
        return source.substring(0, len);
    }

    public static String getRandomCode(String source, int len) {
        Random random = new Random();
        String randomNum = Md5Util.sign(source, (int) (System.currentTimeMillis() / 1000L), "" + random.nextInt(1555));
        String code = "" + encode62ToInt(randomNum);
        int start = random.nextInt(code.length() - len - 1);
        start = start < 1 ? 0 : start;
        return code.substring(start, start + len);
    }

    /**
     * 字符串加星号脱敏
     *
     * @param source
     * @param start
     * @param len
     * @return
     */
    public static String markString(String source, int start, int len) {
        if (StringUtils.isEmpty(source)) {
            return "";
        }
        if (source.length() < len) {
            return source;
        }
        start = start - 1;
        String begin = start > 0 ? source.substring(0, start) : "";
        String end = start < source.length() ? source.substring(start + len) : "";
        for (int i = 0; i < len; i++) {
            begin += "*";
        }
        return begin + end;
    }

    public static String getMarkedIdCard(String idCard) {
        if (StringUtils.isBlank(idCard)) {
            return idCard;
        }
        if (idCard.length() < 15) {
            return idCard;
        }
        int len = idCard.length() - 10;
        idCard = markString(idCard, 7, len);
        return idCard;
    }

    public static String getMarkedPhone(String phone) {
        if (StringUtils.isBlank(phone)) {
            return phone;
        }
        phone = phone.startsWith("86") ? phone.substring(2) : phone;
        int index = phone.length() > 11 ? phone.length() - 11 : 0;
        int len = phone.length() - 5;
        if (phone.length() < 11) {
            len = 6;
        }
        phone = markString(phone, index + 4, len);
        return phone;
    }

    public static String getMarkedName(String name) {
        if (StringUtils.isBlank(name)) {
            return name;
        }
        name = markString(name, 1, name.length() - 1);
        return name;
    }

    private static final String str62keys = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";

    public static long encode62ToInt(String str62) {
        long i10 = 0;

        for (int i = 0; i < str62.length(); i++) {
            double n = str62.length() - i - 1;
            i10 += str62keys.indexOf(str62.charAt(i)) * Math.pow(62, n);
        }
        String temp = leftPad(String.valueOf(i10), 7, '0');
        try {
            i10 = Long.parseLong(temp);
        } catch (Exception e) {
        } finally {
            return i10;
        }
    }
    // 左边补

    public static String leftPad(String s, int size, char padChar) {
        int length = s.length();
        if (length == 0) {
            return s;
        }
        int pads = size - length;
        if (pads <= 0) {
            return s;
        }
        return padding(pads, padChar).concat(s);
    }
    // 填充

    private static String padding(int repeat, char padChar) {
        if (repeat < 0) {
            throw new IndexOutOfBoundsException("Cannot pad a negative amount: " + repeat);
        }
        char[] buf = new char[repeat];
        for (int i = 0; i < buf.length; i++) {
            buf[i] = padChar;
        }
        return new String(buf);
    }

    public static void returnJson(ServletResponse response, Object result) throws IOException {
        OutputStream writer = null;
        try {
            response.reset();
            response.setCharacterEncoding("UTF-8");
            response.setContentType("application/json;charset=UTF-8");
            response.setContentLength(-1);
            writer = response.getOutputStream();
            writer.write(JsonUtil.object2Json(result).getBytes("UTF-8"));
            writer.flush();
            writer.close();
        } catch (IOException e) {
            log.error(e.getLocalizedMessage());
        } finally {
            if (writer != null) {
                writer.close();
            }
        }
    }

    public static <T> T copyObject(Object obj, Class<T> destClass) {
        if (obj == null) {
            return null;
        }
        try {
            T destObj = destClass.newInstance();
            BeanUtils.copyProperties(obj, destObj);
            return destObj;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static int getInt(Integer source) {
        if (source == null) {
            return 0;
        }
        return source.intValue();
    }

    public static int getVersion(String version) {
        try {
            String[] vers = version.split("\\.");
            String newVer = vers[0] + vers[1] + StringUtils.leftPad(vers[2], 2, '0');
            return Integer.parseInt(StringUtils.getDigits(newVer));
        } catch (NumberFormatException e) {
            return 0;
        }
    }

    public static String getTrimStr(String str, int len) {
        if (StringUtils.isEmpty(str)) {
            return str;
        }
        return str.length() > len ? str.substring(0, len - 1) + "..." : str;
    }

    private static Pattern humpPattern = Pattern.compile("[A-Z]");

    /**
     * 驼峰转下划线,效率比上面高
     */
    public static String humpToLine2(String str) {
        Matcher matcher =
                humpPattern.matcher(str);
        StringBuffer sb = new StringBuffer();
        while (matcher.find()) {
            matcher.appendReplacement(sb, "_" + matcher.group(0).toLowerCase());
        }
        matcher.appendTail(sb);
        return sb.toString();
    }

    public static void copyProperties(Object source, Object target) throws BeansException {
        Assert.notNull(source, "Source must not be null");
        Assert.notNull(target, "Target must not be null");
        Class<?> actualEditable = target.getClass();

        PropertyDescriptor[] targetPds = getPropertyDescriptors(actualEditable);
        PropertyDescriptor[] var7 = targetPds;
        int var8 = targetPds.length;

        for (int var9 = 0; var9 < var8; ++var9) {
            PropertyDescriptor targetPd = var7[var9];
            Method writeMethod = targetPd.getWriteMethod();
            if (writeMethod != null) {
                PropertyDescriptor sourcePd = getPropertyDescriptor(source.getClass(), targetPd.getName());
                if (sourcePd != null) {
                    Method readMethod = sourcePd.getReadMethod();
                    if (readMethod != null && ClassUtils.isAssignable(writeMethod.getParameterTypes()[0], readMethod.getReturnType())) {
                        try {
                            if (!Modifier.isPublic(readMethod.getDeclaringClass().getModifiers())) {
                                readMethod.setAccessible(true);
                            }
                            Object value = readMethod.invoke(source);
                            // 判断value是否为空
                            if (value != null) {
                                if (!Modifier.isPublic(writeMethod.getDeclaringClass().getModifiers())) {
                                    writeMethod.setAccessible(true);
                                }
                                writeMethod.invoke(target, value);
                            }
                        } catch (Throwable var15) {
                            throw new FatalBeanException("Could not copy property '" + targetPd.getName() + "' from source to target", var15);
                        }
                    }
                }
            }
        }
    }

    public static boolean isDouble(String str) {
        Pattern pattern = Pattern.compile("-?[0-9]+.*[0-9]*");
        return pattern.matcher(str).matches();
    }

   final static Pattern numP = Pattern.compile("\\d+");
    // 判断一个字符串是否含有数字
    public static boolean hasDigit(String content) {
        boolean flag = false;
        Matcher m = numP.matcher(content);
        if (m.matches()) {
            flag = true;
        }
        return flag;
    }

    public static String getNumbers(String content) {
        Matcher matcher = numP.matcher(content);
        while (matcher.find()) {
            return matcher.group(0);
        }
        return "0";
    }

}
