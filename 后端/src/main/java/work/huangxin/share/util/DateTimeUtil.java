package work.huangxin.share.util;

import org.springframework.util.StringUtils;

import java.time.*;
import java.time.format.DateTimeFormatter;

public class DateTimeUtil {


    private static final String formatPartten = "yyyy-MM-dd HH:mm:ss";
    private static final String formatShortPartten = "yyyy-MM-dd";
    private static final String formatCHShortPartten = "yyyy年MM月dd日";
    private static final String formatTimePartten = "HH:mm";
    private static final String formatDateStrPartten = "yyyyMMdd";
    private static final String formatShortDateStrPartten = "yyMMdd";
    private static final String formatMinutesStr = "yyyyMMddHHmm";
    private static final String formatYearStr = "yyyy";
    private static final String formatYearMonthStr = "yyyy-MM";
    public static LocalDateTime getLocalDateTime(String date) {
        try {
            if (StringUtils.isEmpty(date)) {
                return null;
            }
            if (date.length() < formatPartten.length()) {
                date += " 00:00:00";
            }
            return LocalDateTime.parse(date, DateTimeFormatter.ofPattern(formatPartten));
        } catch (Exception e) {
            return null;
        }
    }

    public static LocalDateTime getAfterLocalDateTime(String date) {
        try {
            if (StringUtils.isEmpty(date)) {
                return null;
            }
            if (date.length() > formatShortPartten.length()) {
                date = date.substring(0, formatShortPartten.length());
            }
            date += " 23:59:59";
            return LocalDateTime.parse(date, DateTimeFormatter.ofPattern(formatPartten));
        } catch (Exception e) {
            return null;
        }
    }


    public static String toLocalDateStr(LocalDate date) {
        try {
            if (date == null) {
                return null;
            }
            return date.format(DateTimeFormatter.ofPattern(formatShortPartten));
        } catch (Exception e) {
            return null;
        }
    }
    public static String toLocalDateStr(LocalDateTime date) {
        try {
            if (date == null) {
                return null;
            }
            return date.format(DateTimeFormatter.ofPattern(formatShortPartten));
        } catch (Exception e) {
            return null;
        }
    }
    public static String toCHLocalDateStr(LocalDate date) {
        try {
            if (date == null) {
                return null;
            }
            return date.format(DateTimeFormatter.ofPattern(formatCHShortPartten));
        } catch (Exception e) {
            return null;
        }
    }
    public static String toLocalYearMonthStr(LocalDateTime date) {
        try {
            if (date == null) {
                return null;
            }
            return date.format(DateTimeFormatter.ofPattern(formatYearMonthStr));
        } catch (Exception e) {
            return null;
        }
    }
    public static String toLocalDateTimeStr(LocalDateTime date) {
        try {
            if (date == null) {
                return null;
            }
            return date.format(DateTimeFormatter.ofPattern(formatPartten));
        } catch (Exception e) {
            return null;
        }
    }

    public static String getTodayStr() {
        return LocalDate.now().format(DateTimeFormatter.ofPattern(formatDateStrPartten));
    }


    public static String getShortTodayStr() {
        return LocalDate.now().format(DateTimeFormatter.ofPattern(formatShortDateStrPartten));
    }
    public static String getLongTodayStr() {
        return LocalDate.now().format(DateTimeFormatter.ofPattern(formatShortPartten));
    }



    public static LocalDate getLocalDate(String date) {
        try {
            if (StringUtils.isEmpty(date)) {
                return null;
            }
            return LocalDate.parse(date, DateTimeFormatter.ofPattern(formatShortPartten));
        } catch (Exception e) {
            return null;
        }
    }




    public static String getHourMinutes(LocalDateTime date) {
        try {
            return date.format(DateTimeFormatter.ofPattern(formatTimePartten));
        } catch (Exception e) {
            return null;
        }
    }
    /**
     * 获取当前时间段按每小时10分钟分段
     * @return
     */
    public static String getTimeMinutes() {
        LocalDateTime now=LocalDate.now().atTime(LocalDateTime.now().getHour(),(LocalDateTime.now().getMinute()/10)*10,0);
        return now.format(DateTimeFormatter.ofPattern(formatMinutesStr));
    }
    /**
     * 是否在范围内
     *
     * @param start
     * @param end
     * @return
     */
    public static boolean inScopeTime(LocalDateTime start, LocalDateTime end) {
        LocalDateTime nowTime = LocalDateTime.now();
        return inScopeTime(nowTime,start,end);
    }

    public static boolean inScopeTime(LocalDateTime nowTime,LocalDateTime start, LocalDateTime end) {
        if(nowTime==null){
            return false;
        }
        return nowTime.isAfter(start) && nowTime.isBefore(end);
    }
    /**
     * 剩余时间
     *
     * @param endTime
     * @return
     */
    public static int leftTime(LocalDateTime endTime) {
        if(endTime==null){
            return 9999;
        }
        Duration duration = Duration.between(LocalDateTime.now(), endTime);
        Long second = duration.toMillis() / 1000L;
        return second.intValue();
    }

    /**
     * 按生日取年龄
     *
     * @param birthday
     * @return
     */
    public static int getAge(LocalDateTime birthday) {
        Period period = Period.between(birthday.toLocalDate(),LocalDateTime.now().toLocalDate());
        return period.getYears();
    }
    public static int getAge(LocalDate birthday) {
        if(birthday==null){
            return 0;
        }
        Period period = Period.between(birthday,LocalDate.now());
        return period.getYears();
    }

    /**
     * 按生日取年
     *
     * @param birthday
     * @return
     */
    public static int getAge(String birthday) {
        LocalDateTime source=getLocalDateTime(birthday);
        if(source==null){
            return 0;
        }
        Period period = Period.between(source.toLocalDate() , LocalDateTime.now().toLocalDate());
        return period.getYears();
    }

    public static String getYear(LocalDateTime dateTime, int size) {
        if (size > 2) {
            return dateTime.format(DateTimeFormatter.ofPattern(formatDateStrPartten)).substring(0, size);
        }
        return dateTime.format(DateTimeFormatter.ofPattern(formatShortDateStrPartten)).substring(0, size);
    }
    public static String getYear() {

        return LocalDate.now().format(DateTimeFormatter.ofPattern(formatYearStr));
    }

    public static String getMonth(LocalDateTime dateTime) {
        return dateTime.format(DateTimeFormatter.ofPattern("MM"));
    }

    public static String getDay(LocalDateTime dateTime) {
        return dateTime.format(DateTimeFormatter.ofPattern("dd"));
    }


    public static String getMonth() {
        return LocalDate.now().format(DateTimeFormatter.ofPattern("MM"));
    }

    public static LocalDateTime getTodayStart() {
        LocalDateTime now= LocalDateTime.now();
        return LocalDateTime.of(now.toLocalDate(), LocalTime.MIN);
    }
    public static LocalDateTime getTodayEnd() {
        LocalDateTime now= LocalDateTime.now();
        return LocalDateTime.of(now.toLocalDate(), LocalTime.MAX);
    }

    public static void main(String[] args) {
        System.out.println(toLocalDateStr(getTodayStart())+"-"+toLocalDateStr(getTodayEnd()));
    }

    public static String getYear(LocalDate startTime) {

        return startTime.format(DateTimeFormatter.ofPattern(formatYearStr));
    }

    public static LocalDateTime getStart(LocalDate selectedDay) {
       return LocalDateTime.of( selectedDay, LocalTime.MIN);
    }

    public static LocalDateTime getEnd(LocalDate selectedDay) {
        return LocalDateTime.of( selectedDay, LocalTime.MAX);
    }

    public static String getDateString(LocalDate dateTime) {
        return dateTime.format(DateTimeFormatter.ofPattern(formatShortPartten));
    }
}
