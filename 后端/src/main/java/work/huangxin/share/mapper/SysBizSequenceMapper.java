package work.huangxin.share.mapper;


import org.apache.ibatis.annotations.Param;
import work.huangxin.share.model.SysBizSequenceEntity;


/**
 * <p>
 * 业务序列号 Mapper 接口
 * </p>
 *
 * @author yu.li
 * @since 2021-01-26
 */
public interface SysBizSequenceMapper {

    SysBizSequenceEntity selectByBizTypeAndDateStr(@Param("bizType") String bizType, @Param("dateStr") String dateStr);

    SysBizSequenceEntity selectByIdForUpdate(@Param("id") long id);

    int updateSequenceById(@Param("currSequence") int currSequence, @Param("id") long id);

    int insert(SysBizSequenceEntity sysBizSequenceEntity);
}
