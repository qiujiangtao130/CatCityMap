package work.huangxin.share.service;

public interface SequenceComponent {

    /**
     * 获取序列
     *
     * @param prefix 前缀
     * @param midFix 中缀
     * @return
     */
    String getSequence(String prefix, String midFix);
}
