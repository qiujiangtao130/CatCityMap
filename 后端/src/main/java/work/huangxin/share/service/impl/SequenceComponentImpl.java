package work.huangxin.share.service.impl;



import org.springframework.stereotype.Service;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionTemplate;
import work.huangxin.share.mapper.SysBizSequenceMapper;
import work.huangxin.share.model.SysBizSequenceEntity;
import work.huangxin.share.service.SequenceComponent;
import work.huangxin.share.util.DateTimeUtil;
import work.huangxin.share.util.ParamsUtil;

import javax.annotation.Resource;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ConcurrentHashMap;

@Service
public class SequenceComponentImpl implements SequenceComponent {

    @Resource
    private SysBizSequenceMapper bizSequenceDAO;
    @Resource
    private TransactionTemplate transactionTemplate;
    /**
     * 本地序列号缓存
     */
    private final Map<String, Queue<Integer>> cache = new ConcurrentHashMap<>();
    private final int cacheSize = 500;

    @Override
    public String getSequence(String prefix, String midFix) {
        // 0、计算当前日期
        String today = DateTimeUtil.getShortTodayStr();
        final String cacheKey = prefix + "_" + today;
        // 1、查询并使用缓存的序列号
        Queue<Integer> todayCacheList = cache.get(cacheKey);
        if (ParamsUtil.collectIsEmpty(todayCacheList)) {
            // 2、如果没有序列号或用尽，则进入中央存储申请
            // 2.1、查询当前日期的序列号存储，
            SysBizSequenceEntity sequence = bizSequenceDAO.selectByBizTypeAndDateStr(prefix, today);
            if (sequence == null) {
                sequence = createNewSequenceRecord(prefix, today);
            }
            final Integer start = sequence.getCurrSequence();
            final Long id = sequence.getId();
            transactionTemplate.execute((TransactionStatus status) -> {
                Integer end = start + cacheSize;
                Queue<Integer> todayNewCacheList = new ArrayBlockingQueue<>(cacheSize);
                for (int i = start; i < end; i++) {
                    todayNewCacheList.add(i);
                }
                // 2.3、 取号并更新
                final int count = bizSequenceDAO.updateSequenceById(end, id);
                if (count < 1) {
                    status.setRollbackOnly();
                    return -1;
                }
                cache.putIfAbsent(cacheKey, todayNewCacheList);
                return 0;
            });
            todayCacheList = cache.get(cacheKey);
        }
        if (ParamsUtil.collectIsEmpty(todayCacheList)) {
            return null;
        }
        final Integer seq = todayCacheList.poll();
        cache.putIfAbsent(cacheKey, todayCacheList);
        // 清掉之前日期的缓存
        cache.keySet().stream().filter(key -> !key.equalsIgnoreCase(cacheKey))
                .forEach(key -> cache.remove(key));

        return new StringBuilder().append(prefix).append(today).append(midFix)
                .append(String.format("%06d", seq % 1000000)).toString();
    }

    /**
     * 存储一条全新的sequence记录
     */
    private SysBizSequenceEntity createNewSequenceRecord(String prefix, String midFix) {
        SysBizSequenceEntity sysBizSequenceDO = new SysBizSequenceEntity();
        sysBizSequenceDO.setBizType(prefix);
        sysBizSequenceDO.setDateStr(midFix);
        sysBizSequenceDO.setCurrSequence(1);
        bizSequenceDAO.insert(sysBizSequenceDO);
        return sysBizSequenceDO;
    }
}