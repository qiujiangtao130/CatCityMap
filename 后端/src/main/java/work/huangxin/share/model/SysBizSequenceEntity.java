package work.huangxin.share.model;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class SysBizSequenceEntity {
    /*  */
    private Long id;

    /*  */
    private LocalDateTime gmtCreate;

    /*  */
    private LocalDateTime gmtModified;

    /* 序列号类型码 */
    private String bizType;

    /* 序列号日期 */
    private String dateStr;

    /* 序列号当前值 */
    private Integer currSequence;

    /*  */
    private Integer deleted;
}