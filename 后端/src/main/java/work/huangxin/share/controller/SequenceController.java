package work.huangxin.share.controller;


import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import work.huangxin.share.service.SequenceComponent;

/**
 * <p>
 * 用户表 前端控制器
 * </p>
 *
 * @author yu.li
 * @since 2020-05-21
 */
@RestController
@RequestMapping("/sequence")
@Slf4j
public class SequenceController {

    @Autowired
    private SequenceComponent sequenceComponent;

    @GetMapping("/generate")
    public String generate(@RequestParam("sequenceType") String sequenceTypeEnum,
                           @RequestParam(value = "midFix", required = false, defaultValue = "") String midFix) {
        return sequenceComponent.getSequence(sequenceTypeEnum, midFix);
    }


}

